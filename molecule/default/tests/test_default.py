import os
import testinfra.utils.ansible_runner


testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('all')


def test_containers(host):
    with host.sudo():
        influxdb = host.docker('influxdb')
        assert influxdb.is_running


def test_influxdb_api(host):
    cmd = host.run('curl --fail http://ics-ans-influxdb-default:8086/ping')
    assert cmd.rc == 0
